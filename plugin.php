<?php
/**
 * Plugin Name: Driv Digtial - UIkit
 * Plugin URI: https://bitbucket.org/drivdigital/dd-uikit
 * Description: UIkit support for WooCommerce.
 * Author: Driv Digital
 * Author URI: https://drivdigital.no/
 * Text Domain: dd-uikit
 * Domain Path: /languages
 * Version: 0.5.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once __DIR__ . '/classes/class-uikit.php';
require_once __DIR__ . '/classes/class-uikit-walker-nav-menu.php';

require_once __DIR__ . '/classes/class-uikit-component-breadcrumb.php';
require_once __DIR__ . '/classes/class-uikit-component-navbar.php';
require_once __DIR__ . '/classes/class-uikit-component-offcanvas.php';

require_once __DIR__ . '/classes/class-uikit-widget-breadcrumb.php';

require_once __DIR__ . '/classes/class-uikit-tabs.php';

add_action( 'init', 'Driv_Digital\UIkit::init' );
Driv_Digital\Uikit_Tabs::setup();
