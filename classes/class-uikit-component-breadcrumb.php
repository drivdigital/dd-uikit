<?php

class UIkit_Component_Breadcrumb {

	public static function get() {
		if ( ! defined( 'WPSEO_VERSION' ) ) {
			return;
		}

		$breadcrumbs = yoast_breadcrumb( '', '', false );
		$breadcrumbs = explode( WPSEO_Options::get( 'breadcrumbs-sep' ), $breadcrumbs );

		if ( count( $breadcrumbs ) < 2 ) {
			return;
		}

		?>
		<section class="uk-section uk-section-xsmall uk-padding-remove-bottom breadcrumbs">
			<div class="uk-container uk-container-small">
				<ul class="uk-breadcrumb uk-overflow-auto" aria-label="breadcrumbs">
					<?php foreach ( $breadcrumbs as $key => $breadcrumb ) :

						$breadcrumb = trim( $breadcrumb );

						end( $breadcrumbs );

						$aria = '';

						// Add aria to the last breadcrumb.
						if ( $key === key( $breadcrumbs ) ) {
							$aria = ' aria-current="page"';
						}

						$before = '';
						$after = '';

						// Wrap breadcrumb in <span> if it's not an anchor.
						if ( ! preg_match( '~^<a.*>.*</a>$~i', $breadcrumb, $matches ) ) {
							$before = sprintf( '<span%s>', $aria );
							$after = '</span>';
						}

						?>

						<li><?php echo sprintf( '%s' . $breadcrumb . '%s', $before, $after ); ?></li>

					<?php endforeach; ?>
				</ul>
			</div>
		</section>
		<?php
	}
}
