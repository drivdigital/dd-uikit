<?php
/**
 * DrivDigital UIKit plugin
 *
 * @package drivdigital\dd-woocommerce-uikit
 */

namespace Driv_Digital;

/**
 *  UIit Drivkraft
 */
class UIkit {

	public static function init() {
		self::uikit_enqueue_scripts();
		self::change_checkout_coupon_form();
		self::wrap_account_navigation();
		self::wrap_customer_details_on_checkout();
		self::my_account();
		self::form_shipping();
		self::form_billing();
		self::form_checkout();
		self::form_login();
		self::form_field_args();
		self::add_tab_support();
		// self::custom_override_checkout_fields(); // @remove when tested
		self::dequeue_woocommerce_styles();
		self::loop_add_to_cart_link();
		self::yikes_woocommerce_direct_link_to_product_tabs();
	}


	public static function uikit_enqueue_scripts() {
		add_action( 'wp_enqueue_scripts', function () {
			wp_enqueue_style( 'uikit-css', plugin_dir_url( __DIR__ ) . 'assets/css/uikit.css' );
			wp_enqueue_script( 'uikit-js', plugin_dir_url( __DIR__ ) . 'assets/js/uikit.min.js' );
			wp_enqueue_script( 'uikit-icons-js', plugin_dir_url( __DIR__ ) . 'assets/js/uikit-icons.min.js' );
		}, 5 );
	}

	public static function single_product_price() {
		ob_start();
		woocommerce_template_single_price();
		$single_price = ob_get_clean();
		echo preg_replace( '/class="price"/', 'class="price uk-text-large uk-text-bold"', $single_price );
	}

	public static function change_checkout_coupon_form() {
		remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
		add_action( 'woocommerce_before_checkout_form', function () {
			ob_start();
			woocommerce_checkout_coupon_form();
			$checkout_coupon_form = ob_get_clean();
			echo preg_replace( '/class="button"/', 'class="button uk-button-primary"', $checkout_coupon_form );
		}, 10 );
	}

	public static function wrap_account_navigation() {
		add_action( 'woocommerce_before_account_navigation', function () {
			?>
			<div class="uk-width-1-4@s">
			<?php
		} );
		add_action( 'woocommerce_after_account_navigation', function () {
			?>
			</div>
			<?php
		} );
	}

	public static function wrap_customer_details_on_checkout() {
		add_action( 'woocommerce_checkout_before_customer_details', function () {
			?>
			<div class="uk-background-muted uk-padding">
			<?php
		}, 20 );
		add_action( 'woocommerce_checkout_after_customer_details', function () {
			?>
			</div>
			<?php
		}, 20 );
	}

	/**
	 * [woocommerce_template_loop_product_link_open Add classes to loop open]
	 */
	public static function woocommerce_template_loop_product_link_open() {
		// @todo - Add customiser option for enabled card style for products
		// If card is disabled
		$additional_classes = apply_filters( 'uikit_product_wrapper_classes', 'uk-flex uk-flex-column uk-flex-between uk-card uk-width-1-1' );
		echo '<div style="height: 100%;" class="' . esc_html( $additional_classes ) . '">';
		woocommerce_template_loop_product_link_open();
	}

	/**
	 * Woocommerce loop closure html
	 */
	public static function woocommerce_template_loop_product_link_close() {
		woocommerce_template_loop_product_link_close();
		echo '</div>';
	}

	/**
	 * Add additional UIkit classes to the product title
	 */
	public static function woocommerce_template_loop_product_title() {
		ob_start();
		woocommerce_template_loop_product_title();
		$product_title = ob_get_clean();
		$classes = apply_filters( 'uikit_loop_item_classes', 'uk-text-small uk-margin-medium-top uk-text-left' );
		echo preg_replace( '/class="(.*)"/', 'class="$1 ' . $classes . '"', $product_title );
	}

	public static function my_account() {
		add_action( 'woocommerce_before_template_part', function ( $template_name ) {
			if ( 'myaccount/my-account.php' !== $template_name ) {
				return;
			}
			ob_start( array( 'self', 'my_account_callback' ) );
		} );
		add_action( 'woocommerce_after_template_part', function ( $template_name ) {
			if ( 'myaccount/my-account.php' !== $template_name ) {
				return;
			}
			ob_end_flush();
		} );
	}

	public static function my_account_callback( $html ) {
		return str_replace( '<div class="woocommerce-MyAccount-content">', '<div class="woocommerce-MyAccount-content uk-width-3-4">', $html );
	}

	public static function form_shipping() {
		add_action( 'woocommerce_before_template_part', function ( $template_name ) {
			if ( 'checkout/form-shipping.php' !== $template_name ) {
				return;
			}
			ob_start( array( 'self', 'form_shipping_callback' ) );
		} );
		add_action( 'woocommerce_after_template_part', function ( $template_name ) {
			if ( 'checkout/form-shipping.php' !== $template_name ) {
				return;
			}
			ob_end_flush();
		} );
	}

	public static function form_shipping_callback( $html ) {
		$pattern = [
			'<div class="woocommerce-shipping-fields__field-wrapper">',
			'woocommerce-additional-fields',
		];
		$replacement = [
			'<div class="woocommerce-shipping-fields__field-wrapper uk-child-width-1-1">',
			'woocommerce-additional-fields'
		];
		return str_replace( $pattern, $replacement, $html );
	}

	public static function form_billing() {
		add_action( 'woocommerce_before_template_part', function ( $template_name ) {
			if ( 'checkout/form-billing.php' !== $template_name ) {
				return;
			}
			ob_start( array( 'self', 'form_billing_callback' ) );
		} );
		add_action( 'woocommerce_after_template_part', function ( $template_name ) {
			if ( 'checkout/form-billing.php' !== $template_name ) {
				return;
			}
			ob_end_flush();
		} );
	}

	public static function form_billing_callback( $html ) {
		return str_replace( '<div class="woocommerce-billing-fields__field-wrapper">', '<div class="woocommerce-billing-fields__field-wrapper uk-child-width-1-1">', $html );
	}

	// Add UIkit grid support to customer details section in checkout form
	// woocommerce/templates/checkout/form-checkout.php
	public static function form_checkout() {
		add_action( 'woocommerce_before_template_part', function ( $template_name ) {
			if ( 'checkout/form-checkout.php' !== $template_name ) {
				return;
			}
			ob_start( array( 'self', 'form_checkout_callback' ) );
		} );
		add_action( 'woocommerce_after_template_part', function ( $template_name ) {
			if ( 'checkout/form-checkout.php' !== $template_name ) {
				return;
			}
			ob_end_flush();
		} );
	}

	public static function form_checkout_callback( $html ) {
		$pattern = [
			'col2-set',
			'id="customer_details"',
			// '<div id="order_review" class="woocommerce-checkout-review-order">'
		];
		$replacement = [
			'col2-set uk-child-width-1-2@s',
			'id="customer_details"',
			// '<div id="order_review" class="woocommerce-checkout-review-order uk-child-width-1-2@s">'
		];
		return str_replace( $pattern, $replacement, $html );
	}

	public static function form_login() {
		add_action( 'woocommerce_before_template_part', function ( $template_name ) {
			if ( 'myaccount/form-login.php' !== $template_name ) {
				return;
			}
			ob_start( array( 'self', 'form_login_callback' ) );
		} );
		add_action( 'woocommerce_after_template_part', function ( $template_name ) {
			if ( 'myaccount/form-login.php' !== $template_name ) {
				return;
			}
			ob_end_flush();
		} );
	}

	public static function form_login_callback( $html ) {
		$pattern = [
			'col2-set',
			'id="customer_login"',
			// '<div id="order_review" class="woocommerce-checkout-review-order">'
		];
		$replacement = [
			'col2-set uk-child-width-1-2@s',
			'id="customer_login"',
			// '<div id="order_review" class="woocommerce-checkout-review-order uk-child-width-1-2@s">'
		];
		return str_replace( $pattern, $replacement, $html );
	}

	public static function form_field_args() {
		add_filter( 'woocommerce_form_field_args', function ( $args, $key, $value = null ) {

			switch ( $args['type'] ) {

				case 'select' :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'uk-select' );
					//$args['custom_attributes']['data-plugin'] = 'select2';
					$args['label_class'] = array( 'uk-form-label' );
					$args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  );
				break;

				case 'country' :
					$args['class'][] = 'field single-country';
					$args['label_class'] = array( 'uk-form-label' );
				break;

				case 'state' :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'input' );
					//$args['custom_attributes']['data-plugin'] = 'select2';
					$args['label_class'] = array( 'uk-form-label' );
					$args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  );
				break;

				case 'password' :
				case 'text' :
				case 'email' :
				case 'tel' :
				case 'number' :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'uk-input' );
					$args['label_class'] = array( 'uk-form-label' );
				break;

				case 'textarea' :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'uk-textarea' );
					$args['label_class'] = array( 'uk-form-label' );
				break;

				case 'checkbox' :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'uk-checkbox' );
					$args['label_class'] = array( 'uk-form-label' );
				break;

				case 'radio' :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'uk-radio' );
					$args['label_class'] = array( 'uk-form-label' );
				break;

				default :
					$args['class'][] = 'uk-form-controls';
					$args['input_class'] = array( 'uk-input' );
					$args['label_class'] = array( 'uk-form-label' );
				break;
			}

			return $args;
		}, 10, 3 );
	}

	/**
	 * Add UIkit tab support to standard WooCommerce tabs
	 * See: woocommerce/templates/content-single-product.php
	 */
	public static function add_tab_support() {
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
		add_action( 'woocommerce_after_single_product_summary', __CLASS__ . '::uikit_output_product_data_tabs', 12 );
	}

	/**
	 * Output the product tabs.
	 */
	public static function uikit_output_product_data_tabs() {
		ob_start();

		woocommerce_output_product_data_tabs();
		$output_product_data_tabs = ob_get_clean();

		$pattern = [
			'/(woocommerce-tabs wc-tabs-wrapper)/',
			'/(role="tablist")/',
		];

		$replacement = [
			'$1 uk-margin-medium',
			'$1 uk-tab',
		];

		echo preg_replace( $pattern, $replacement, $output_product_data_tabs ); // XSS ok.
	}

	public static function custom_override_checkout_fields() {
		add_filter( 'woocommerce_checkout_fields', function ( $fields ) {
			$fields['billing']['billing_first_name']['class'][] = 'uk-width-1-2';
			$fields['billing']['billing_last_name']['class'][] = 'uk-width-1-2';

			if ( count( WC()->countries->get_allowed_countries() ) < 2 ) {
				$fields['billing']['billing_country']['class'][] = 'uk-hidden';
			}

			$fields['billing']['billing_address_1']['placeholder'] = ''; // TODO doesn't work.
			unset( $fields['billing']['billing_address_2'] );
			$fields['billing']['billing_postcode']['class'][] = 'uk-width-1-3';
			$fields['billing']['billing_city']['class'][] = 'uk-width-2-3';
			$fields['billing']['billing_phone']['class'][] = 'uk-width-1-3';
			$fields['billing']['billing_email']['class'][] = 'uk-width-2-3';

			$fields['shipping']['shipping_first_name']['class'][] = 'uk-width-1-2';
			$fields['shipping']['shipping_last_name']['class'][] = 'uk-width-1-2';

			if ( count( WC()->countries->get_shipping_countries() ) < 2 ) {
				$fields['shipping']['shipping_country']['class'][] = 'uk-hidden';
			}

			$fields['shipping']['shipping_address_1']['placeholder'] = ''; // TODO doesn't work.
			unset( $fields['shipping']['shipping_address_2'] );
			$fields['shipping']['shipping_postcode']['class'][] = 'uk-width-1-3';
			$fields['shipping']['shipping_city']['class'][] = 'uk-width-2-3';

			return $fields;
		} );
	}

	public static function dequeue_woocommerce_styles() {
		add_filter( 'woocommerce_enqueue_styles', function ( $enqueue_styles ) {

			// Remove the gloss
			unset( $enqueue_styles['woocommerce-general'] );

			// Remove the layout
			unset( $enqueue_styles['woocommerce-layout'] );

			// Remove the smallscreen optimisation
			unset( $enqueue_styles['woocommerce-smallscreen'] );

			return $enqueue_styles;
		} );
	}

	public static function loop_add_to_cart_link() {
		add_filter( 'woocommerce_loop_add_to_cart_link', function ( $html ) {
			return '<span class="add_to_cart_button-container uk-text-center uk-text-left@s">' . $html . '</span>';
		} );
	}

	// Allows you to create custom URLs to activate product tabs by default, directly from the URL
	// http://www.remicorson.com/access-woocommerce-product-tabs-directly-via-url/
	public static function yikes_woocommerce_direct_link_to_product_tabs() {
		add_action( 'wp_footer', function () {
			if ( ! is_product() ) {
				return;
			}
			?>
			<script>
				jQuery(function ( $ ) {

					$( 'a[href^="#"]' ).on( 'click', function () {

						$( window ).trigger( 'hashchange' );

					} );

					$( window ).on( 'hashchange', function () {

						if ( ! window.location.hash ) {
							return;
						}

						var tabPrefix = '#tab-';
						var hash = window.location.hash.replace( '#', '' );

						// Get tab ID from URL hash
						var tab = tabPrefix + hash;

						// Do not continue if selected tab content does not exist
						// Prevents showing empty tab content if URL hash does not refer to tab ID
						if ( ! $( tab ).length ) {
							return;
						}

						// Set only selected tab as active
						$( '.uk-tab li' ).removeClass( 'active uk-active' );
						$( 'a[href="' + tab + '"]' ).parent( 'li' ).addClass( 'active uk-active' );

						// Show only selected tab content
						$( 'div[id^="tab-"]' ).hide();
						$( tab ).show();

						$( 'html, body' ).animate( {
							scrollTop: $( tabPrefix + 'title-' + hash ).offset().top,
						}, 250 );

					}).trigger( 'hashchange' );

					// Update URL hash when tab is clicked
					$( '.uk-tab li a' ).on( 'click', function () {
						window.location.hash = $( this ).parent( 'li' ).attr( 'id' ).replace( 'tab-title-', '' );
					});

				});
			</script>
			<?php
		}, 100 );
	}
}
