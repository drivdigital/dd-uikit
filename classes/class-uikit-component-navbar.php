<?php

class UIkit_Component_Navbar {

	public static function get() {
		?>
		<nav class="uk-navbar-container uk-navbar-transparent uk-light uk-background-secondary">
			<div class="uk-container uk-container-small">
				<div uk-navbar>
					<div class="uk-navbar-left">
						<?php
						wp_nav_menu(
							array(
								'container'   => false,
								'menu_class'  => 'uk-navbar-nav uk-visible@m',
								'fallback_cb' => false,
								'menu_id'     => 'main-menu-navbar',
								'walker'      => new UIkit_Walker_Nav_Menu(),
							)
						);
						?>
						<ul class="uk-navbar-nav uk-hidden@m">
							<li><a href="#offcanvas-nav" uk-toggle uk-icon="menu"></a></li>
						</ul>
					</div>
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav">
							<li>
								<?php if ( is_user_logged_in() ) : ?>
									<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php esc_html_e( 'My Account', 'woocommerce' ); ?>"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
 								<?php else : ?>
									<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php esc_html_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></a>
								<?php endif; ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<?php
	}
}
