<?php
/**
 * UIKit Tabs
 *
 * @package drivdigital\dd-woocommerce-uikit
 */

namespace Driv_Digital;

/**
 * UIKit Tabs
 */
class Uikit_Tabs {

	/**
	 * Setup
	 */
	public static function setup() {
		add_filter( 'dd_gutenberg_tabs_classes', __CLASS__ . '::tab_classes' );
	}

	/**
	 * Tab Classes
	 *
	 * @param  array $classes Classes.
	 * @return array
	 */
	public static function tab_classes( $classes ) {
		$classes['navigation']['class'] .= ' uk-tab';
		$classes['nav_item']['active']  .= ' uk-active';
		return $classes;
	}
}
