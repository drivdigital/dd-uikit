<?php

class UIkit_Widget_Breadcrumb extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'description' => 'UIkit Breadcrumb',
		);
		parent::__construct( 'uikit_breadcrumb_widget', 'UIkit Breadcrumb', $widget_ops );
	}

	public function widget( $args, $instance ) {
		UIkit_Component_Breadcrumb::get();
	}
}

add_action( 'widgets_init', function () {
	register_widget( 'UIkit_Widget_Breadcrumb' );
});
