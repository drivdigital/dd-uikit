<?php

class UIkit_Walker_Nav_Menu extends Walker_Nav_Menu {

	protected $type;

	protected $types = [
		'navbar' => [
			'start_lvl' => '<div class="uk-navbar-dropdown uk-navbar-dropdown-width-2" uk-dropdown="offset: 0"><ul class="uk-nav uk-navbar-dropdown-nav">',
			'end_lvl' => '</ul></div>',
		],
		'offcanvas-nav' => [
			'start_lvl' => '<ul class="uk-nav-sub">',
			'end_lvl' => '</ul>',
		],
	];

	public function __construct( $type = 'navbar' ) {
		$this->type = $type;
	}

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= $this->types[$this->type]['start_lvl'];
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= $this->types[$this->type]['end_lvl'];
    }
}

// TODO active item

// TODO add class="uk-parent" if li has children
