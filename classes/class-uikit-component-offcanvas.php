<?php

class UIkit_Component_Offcanvas {

	public static function get() {
		?>
		<div id="offcanvas-nav" uk-offcanvas="overlay: true">
			<div class="uk-offcanvas-bar">
				<button class="uk-offcanvas-close" type="button" uk-close></button>
				<?php
				wp_nav_menu(
					array(
						'container'   => false,
						'menu_class'  => 'uk-nav uk-nav-default',
						'fallback_cb' => false,
						'menu_id'     => 'main-menu-offcanvas-nav',
						'walker'      => new UIkit_Walker_Nav_Menu( 'offcanvas-nav' ),
					)
				);
				?>
			</div>
		</div>
		<?php
	}
}
